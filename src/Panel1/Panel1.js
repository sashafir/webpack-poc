import React, { Component } from 'react';
import './ui.css';
import RelativeDate from '../Dates/RelativeDate'

export default class Panel1 extends Component
{
    render()
    {   return (
          <div className="Panel1" >
              <i className="Drawer-title">Panel1 React Component loaded dynamically.</i>
              RelativeDate: <RelativeDate/>
          </div>
        );
    }

}
