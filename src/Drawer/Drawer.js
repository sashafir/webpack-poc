import React, { Component } from 'react';
import pic from './drawer1.svg';
import './ui.css';
import ImportError from '../Error/ImportError'; // error handling/messaging modules should be static dependency in JS module heading

export default class Drawer extends Component
{
    constructor()
    {   super(...arguments);
        this.state =        // setState({key:val}) will modify this variable and redraw the component
            {   isOpened :0 // flag for collapsing drawer
            ,   Content  :0 // Panel1 module when loaded
            };
    }
    render()
    {   let Content = this.state.Content;
        return (
          <div className="Drawer" >
            <p><a href="https://bitbucket.org/sashafir/webpack-poc/src/f4bdd88ada28b3f3bd791c707ca5fa131c4d8256/src/Drawer/Drawer.js?at=master&fileviewer=file-view-default"
                >Drawer</a> demo component shows the dynamic load of <a href="https://bitbucket.org/sashafir/webpack-poc/src/f4bdd88ada28b3f3bd791c707ca5fa131c4d8256/src/Panel1/Panel1.js?at=master&fileviewer=file-view-default"
                >Panel1</a> component on click event. This allows to minimize the page JS during initial load and load heavy modules on demand only.
            </p>
            <header className="header" onClick={this.toggleDrawer.bind(this)}>
                <img src={pic} className="Drawer-pic" alt="drawer" />
                <b className="Drawer-title">Click to load React Component asynchronously.</b>
                <p>To simulate the error, load page, then in dev tools/network turn on "Offline" and click to replicate the load failure.<br/>
                    Then turn off "Offline" and re-open the drawer to see whether retry is successful.
                </p>
            </header>
            <div className={this.state.isOpened ?'Drawer-opened':'Drawer-collapsed'}>
              { Content ? <Content /> : 'Loading...' }
            </div>
          </div>
        );
    }
    toggleDrawer(ev)
    {
        ev.preventDefault();

        this.setState({isOpened : !this.state.isOpened});
        import('../Panel1/Panel1') // async module loading
        .then( ({default:Panel1}) => this.setState({ Content:Panel1 }) // will trigger redrawing
             , err =>this.setState({ Content:ImportError }) ); // will trigger redrawing
    }
}

