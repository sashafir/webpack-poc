import React, { Component } from 'react';

export default class ImportError extends Component
{
    render()
    {   return (
          <div className="error" >
              We are experiencing technical difficulties. Please try again a bit later of call customer service.<br/>
              <i>Loading error.</i><br/>
              The error messages should be an explicit dependency (loaded in JS module import heading) to be available
              disregarding of network or JS loader state.

          </div>
        );
    }

}