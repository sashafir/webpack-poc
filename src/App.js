import React, { Component } from 'react';
import Drawer from './Drawer/Drawer';
import logo from './logo.svg';
import './App.css';

class App extends Component
{
    render()
    {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
                <Drawer/>
                <h2>Basic principles </h2>
                <ul>
                    <li>The project bootstrapping uses <a
                        href="https://github.com/facebookincubator/create-react-app">create-react-app</a> CLI.
                        See <a
                            href="https://bitbucket.org/sashafir/webpack-poc/src/1e492fdd662b?at=master&mode=edit&fileviewer=file-view-default"
                        >README.md</a> for project SDLC details.
                    </li>
                    <li><a href="https://webpack.js.org/">WebPack</a> is the primary dependency manager.
                        Details TBD.
                        CLI provides default config sufficient to run in development environment and production build.
                    </li>
                    <li><a href="https://reactjs.org/docs/react-component.html">React.Component</a> is the base for
                        React based components.
                        <ul>
                            <li><a
                                href="https://bitbucket.org/sashafir/webpack-poc/src/f4bdd88ada28b3f3bd791c707ca5fa131c4d8256/src/Drawer/Drawer.js?at=master&fileviewer=file-view-default"
                            >Drawer</a> demo component shows the dynamic load of
                            </li>
                            <li><a
                                href="https://bitbucket.org/sashafir/webpack-poc/src/f4bdd88ada28b3f3bd791c707ca5fa131c4d8256/src/Panel1/Panel1.js?at=master&fileviewer=file-view-default"
                            >Panel1</a> component on click event. This allows to minimize the page JS during initial
                                load and load heavy modules on demand only.
                            </li>
                            <li>And static dependency for Error handlers and messaging (Error/ImportError.js)</li>
                        </ul>
                    </li>
                    <li>The date <span data-jmh-mid="Dates/RelativeDate.js">Loading...</span>
                        <ul>
                            <li>Note the second bundle is loaded with <b>RelativeDate</b> and <b>date-fns</b> only when requested. </li>
                            <li>Same in <a href="build/index.html"> production build</a>. </li>
                        </ul>
                    </li>
                    <li>Common libs integration: <b>date-fns</b>. Init by NPM install. Use by ES6 import, dynamic import, AMD.</li>
                    <li>disable bundling leaving packaged individual JS/css is unnecessary due to automatic recognition of dynamic load by WebPack build and allocating dynamic sub-bundles.</li>
                </ul>
                <h3>ToDo</h3>
                <ul>

                    <li>AMD integration. Sample of shim for input type=date with jquery-ui datepicker in IE, also in dynamically added by React content. Click to show DateSelect bellow as a sample.</li>
                    <li>WidgetLoader</li>
                    <li>WebComponent</li>
                    <li>AEM component</li>
                    <li>React.Component loaded by WidgetLoader</li>
                    <li><a href="http://cssnext.io/">PostCSS-cssnext</a> samples for mixins and variables</li>
                </ul>
            </div>
        );
    }
}

export default App;
